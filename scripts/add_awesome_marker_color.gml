///add_awesome_marker(text, color)
var text = argument0;
var color = argument1;
var inst = add_awesome_marker(text);
inst.color = color;
