///collide_in_y(obj1, obj2)
obj1 = argument0;
obj2 = argument1;

var other_real_x = obj1.x - (obj1.sprite_width / 2);
var this_real_x = obj2.x - (obj2.sprite_width / 2);

var other_width = obj1.sprite_width;
var this_width = obj2.sprite_width;

var collides = true;

if(other_real_x > (this_real_x + this_width)) {
    collides = false;
} else if ((other_real_x + other_width) < this_real_x) {
    collides = false;
}

return collides;
