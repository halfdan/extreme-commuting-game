///compact_buffer(buff)
var buff = argument0;

var alignment = buffer_get_alignment(buff);
var pos = buffer_tell(buff);

var size = pos * alignment;

buffer_resize(buff, size);
