///console_input_letter_select(letter)
var letter = argument0;

var max_size = console_input_max_text_size();
var text_size = string_length(text);

var can_add = max_size > text_size;

if(letter == "ready") {
    visibility = console_input_ready(text);
} else if(letter == "back") {
    var letters = string_length(text);
    text = string_delete(text, letters, 1);
} else if(letter == "shift") {
    shift = !shift;
} else if(letter == "space" and can_add) {
    text += ' ';
} else if(can_add) {
    if(shift) {
        letter = string_upper(letter);
    }
    text += letter;
}
