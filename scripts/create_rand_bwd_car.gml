///create_rand_bwd_car(type_to_set)
var type_to_set = argument0;
var start = obj_backward_car_start;

if("side" == type_to_set) {
    start = obj_backward_left_car_start;
}

var rnd = random(100);
var inst;

var rand_x = irandom(5) -2;

if (rnd >= 94 && rnd <= 100) {
    inst = instance_create(start.x + rand_x, CAR_SPAWN_POSITION_Y, obj_back_police_car);
    
    if (random(100) > 50) {
        inst.pursuit = true;
    }
} else {
    inst = instance_create(start.x + rand_x, CAR_SPAWN_POSITION_Y, obj_back_car1);
    with(inst) {
        var car_num = irandom(6);
        sprite_index = asset_get_index("spr_car_" + string(car_num));
    }
}

with(inst) {
    type = type_to_set;
}
