/// get_level_data_entry(entry_name)
var entry_name = argument0;

if (obj_levels_controller.level_data == LEVEL_RANDOM) {
    return noone;
}

return ds_map_find_value(obj_levels_controller.level_data, entry_name);
