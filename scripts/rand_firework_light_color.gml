/// rand_firework_light_color()
var rand = irandom_range(0, 3);
var color = c_white;

if(rand == 0) {
    color = c_white;
} else if(rand == 1) {
    color = c_yellow;
} else if(rand == 2) {
    color = c_orange;
} else if(rand == 3) {
    color = c_silver;
}

return color;
