/// find_font_by_id(font_id)
var font_id = argument0;

if (font_id == FONT_DIGITS) {
    return fnt_digits;
}
if (font_id == FONT_CHUNK_FIVE_20) {
    return fnt_chunk_five_20;
}
if (font_id == FONT_CHUNK_FIVE_30) {
    return fnt_chunk_five_30;
}
if (font_id == FONT_CHUNK_FIVE_40) {
    return fnt_chunk_five_40;
}
if (font_id == FONT_CHUNK_FIVE_50) {
    return fnt_chunk_five_50;
}
