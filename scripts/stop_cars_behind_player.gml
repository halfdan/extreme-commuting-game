/// stop_cars_behind_player()

var count = instance_number(obj_forward_car);

for(var i = 0; i < count; i++) {
    var inst = instance_find(obj_forward_car, i);
    if(inst.y > obj_car_player.y) {
        inst.is_alive = false;
    }
}
