/// create_and_save_particle(particle_type_id, x, y, number, life_min, life_max)
var particle_type_id = argument0;
var xx = argument1;
var yy = argument2;
var number = argument3;
var life_min = argument4;
var life_max = argument5;

create_particle(particle_type_id, x, y, number, life_min, life_max)

buffer_write(obj_replay_manager.replay_buffer, buffer_u8, REP_TYPE_PARTICLE);

buffer_write(obj_replay_manager.replay_buffer, buffer_u8, particle_type_id);
buffer_write(obj_replay_manager.replay_buffer, buffer_u16, xx);
buffer_write(obj_replay_manager.replay_buffer, buffer_u16, yy);
buffer_write(obj_replay_manager.replay_buffer, buffer_u8, number);
buffer_write(obj_replay_manager.replay_buffer, buffer_f32, life_min);
buffer_write(obj_replay_manager.replay_buffer, buffer_f32, life_max);
