/// button_update(button_id)
var _map = ds_map_find_value(global._buttonsMap, argument0);

var _type   = ds_map_find_value(_map, "type");
var _sprite = ds_map_find_value(_map, "sprite");
var _view   = ds_map_find_value(_map, "view");
var _x      = ds_map_find_value(_map, "x");
var _y      = ds_map_find_value(_map, "y");
var _scale  = ds_map_find_value(_map, "scale");
var _spr_w  = sprite_get_width(_sprite) * _scale;
var _spr_h  = sprite_get_height(_sprite) * _scale;
if _view > -1 {
    _x += view_xview[_view];
    _y += view_yview[_view];
}

var _x1 = _x - _spr_w/2;
var _x2 = _x + _spr_w/2;
var _y1 = _y - _spr_h/2;
var _y2 = _y + _spr_h/2;

var _mx;
var _my;
var _mstate;
var _in_rect;

// DPAD
if _type == 0 {
    var _stl = 0;
    var _str = 0;
    var _stu = 0;
    var _std = 0;
    
    for (_d = 0; _d < 4; _d++) {
        _mx = device_mouse_x(_d);
        _my = device_mouse_y(_d);
        _pd = point_direction(_x, _y, _mx, _my);
        _in_rect = point_in_rectangle(_mx, _my, _x1, _y1, _x2, _y2);
        
        _mstate = 0;
        if device_mouse_check_button_pressed(_d, mb_left)   _mstate = 1;
        if device_mouse_check_button(_d, mb_left)           _mstate = 2;
        if device_mouse_check_button_released(_d, mb_left)  _mstate = 3;

        // RIGHT
        _md = 22.5;
        if _in_rect && (_pd < 90-_md || _pd > 270+_md) {
             _str = _mstate;
        }
        // LEFT
        if _in_rect && (_pd > 90+_md && _pd < 270-_md) {
             _stl = _mstate;
        }
        // UP
        if _in_rect && (_pd > 0+_md && _pd < 180-_md) {
             _stu = _mstate;
        }
        // DOWN
        if _in_rect && (_pd > 180+_md && _pd < 360-_md) {
             _std = _mstate;
        }
    }
    
    ds_map_replace(_map, "state_left", _stl);
    ds_map_replace(_map, "state_right", _str);
    ds_map_replace(_map, "state_up", _stu);
    ds_map_replace(_map, "state_down", _std);
}
// STICK
if _type == 1 {
    var _device = ds_map_find_value(_map, "device");
    var _radius = ds_map_find_value(_map, "radius");
    var _dir = ds_map_find_value(_map, "direction");
    var _dis = ds_map_find_value(_map, "distance");
    if _device == -1 {
        for (_d = 0; _d < 4; _d++) {
            _mx = device_mouse_x(_d);
            _my = device_mouse_y(_d);
            if point_distance(_x, _y, _mx, _my) < _radius {
                if device_mouse_check_button_pressed(_d, mb_left) {
                    _device = _d;
                }
                if device_mouse_check_button(_d, mb_left) {
                    _device = _d;
                }
            }
        }
    }
    if _device != -1 {
        _mx = device_mouse_x(_device);
        _my = device_mouse_y(_device);
        _dir = point_direction(_x, _y, _mx, _my);
        _dis = point_distance(_x, _y, _mx, _my);
        if device_mouse_check_button_released(_device, mb_left) {
            _device = -1;
        }
    }
    _dis -= _radius / 5; 
    if _dis < 0 _dis = 0;
    if _dis > _radius _dis = _radius;
    ds_map_replace(_map, "direction", _dir);
    ds_map_replace(_map, "distance", _dis);
    ds_map_replace(_map, "device", _device);
    ds_map_replace(_map, "state_x", lengthdir_x(_dis / _radius, _dir));
    ds_map_replace(_map, "state_y", lengthdir_y(_dis / _radius, _dir));
}
// SINGULAR 
if _type == 2 {
    var _key   = ds_map_find_value(_map, "key");
    var _state = 0;
    for (_d = 0; _d < 4; _d++) {
        _mx = device_mouse_x(_d);
        _my = device_mouse_y(_d);
        if point_in_rectangle(_mx, _my, _x1, _y1, _x2, _y2) {
            if device_mouse_check_button_pressed(_d, mb_left) {
                _state = 1;
                keyboard_key_press(_key);
            }
            if device_mouse_check_button(_d, mb_left) {
                _state = 2;
            }
            if device_mouse_check_button_released(_d, mb_left) {
                _state = 3;
                keyboard_key_release(_key);
            }
        }
    }
    ds_map_replace(_map, "state", _state);
}
