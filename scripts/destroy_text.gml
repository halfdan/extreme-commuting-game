/// destroy_text(text_id)
var text_id = argument0;

var pos = ds_map_find_value(obj_replay_manager.text_positions, text_id);

if (!is_undefined(pos)) {
    ds_list_replace(obj_replay_manager.texts, pos, undefined);
    ds_map_replace(obj_replay_manager.text_positions, text_id, undefined);
}
