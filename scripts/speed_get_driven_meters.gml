///speed_get_driven_meters(vel)
var vel = argument0;

return vel * 0.05;
