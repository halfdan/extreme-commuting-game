/// update_text(text, font_color, x, y, alpha, text_id)
var text_value = argument0;
var font_color = argument1;
var xx = argument2;
var yy = argument3;
var alpha = argument4;
var text_id = argument5;

var old_text_pos = ds_map_find_value(obj_replay_manager.text_positions, text_id);

if (text_value == TEXT_DESTROY_MARKER) {

    ds_map_replace(obj_replay_manager.text_positions, text_id, undefined);
    ds_list_replace(obj_replay_manager.texts, old_text_pos, undefined);

    return noone;
}

if (!is_undefined(old_text_pos)) {

    var text_array = ds_list_find_value(obj_replay_manager.texts, old_text_pos);
    
    if(text_value != TEXT_UNCHANGED_MARKER) {
        text_array[TEXT_VALUE] = text_value;
    }
    if(font_color != TEXT_UNCHANGED_MARKER) {
        text_array[TEXT_COLOR] = font_color;
    }
    if(xx != TEXT_UNCHANGED_MARKER) {
        text_array[TEXT_X] = xx;
    }
    if(yy != TEXT_UNCHANGED_MARKER) {
        text_array[TEXT_Y] = yy;
    }
    if(alpha != TEXT_UNCHANGED_MARKER) {
        text_array[TEXT_ALPHA] = alpha;
    }
    
    ds_list_replace(obj_replay_manager.texts, old_text_pos, text_array);

}
