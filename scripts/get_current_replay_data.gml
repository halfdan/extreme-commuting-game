/// get_current_replay_data()

if (ds_list_size(obj_replay_manager.replay_data) - 1 < obj_replay_manager.current_step + 1) {
    var data_map = ds_map_create();
    ds_list_add(obj_replay_manager.replay_data, data_map);
}

return ds_list_find_value(obj_replay_manager.replay_data, obj_replay_manager.current_step + 1);
