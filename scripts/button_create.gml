/// button_create(button_type,key,sprite,view,x,y,scale)

var _t = ds_map_find_value(global._buttonsMap, "total");
var _map = ds_map_create();
ds_map_add(global._buttonsMap, _t, _map);
ds_map_replace(global._buttonsMap, "total", _t+1);

ds_map_add(_map, "type",    argument0);
ds_map_add(_map, "key",     argument1);
ds_map_add(_map, "sprite",  argument2);
ds_map_add(_map, "view",    argument3);
ds_map_add(_map, "x",       argument4);
ds_map_add(_map, "y",       argument5);
ds_map_add(_map, "scale",   argument6);

switch (argument0) {
    case 0:// DPAD
        ds_map_add(_map, "state_left", 0);
        ds_map_add(_map, "state_right", 0);
        ds_map_add(_map, "state_up", 0);
        ds_map_add(_map, "state_down", 0);
        break;
    case 1:// STICK
        ds_map_add(_map, "state_x", 0);
        ds_map_add(_map, "state_y", 0);
        ds_map_add(_map, "distance", 0);
        ds_map_add(_map, "direction", 0);
        ds_map_add(_map, "radius", argument6);
        ds_map_add(_map, "device", -1);
        break;
    case 2:// SINGLE
        ds_map_add(_map, "state", 0);
        break;
}

return _t;
