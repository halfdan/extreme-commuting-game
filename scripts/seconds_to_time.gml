///seconds_to_time(seconds)
var seconds = argument0;

if(seconds < 60) {

    if(seconds < 10) {
        return "00:0" + string(round(seconds));
    } else {
        return "00:" + string(round(seconds));
    }
}

var minutes = floor(seconds / 60);
var seconds_left = round(seconds % 60);

var text = "";

if (minutes < 10) {
    text = "0";
}

text += string(minutes) + ":";

if(seconds_left < 10) {
    text += "0";
}

text += string(seconds_left);

return text;
