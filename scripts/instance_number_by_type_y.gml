///instance_number_by_type_y(obj, type, y)
var obj = argument0;
var type = argument1;
var yy = argument2;

var i;
var count = 0;
for (i = 0; i < instance_number(obj); i += 1) {
    inst = instance_find(obj, i);
    if(inst.type == type && (inst.y < yy && inst.y > WAIT_POSITION)) {
        count++;
    }
}

return count;
