///keep_aspect_ratio_for_gui()
var base_w = 800;
var base_h = 600;

var current_w = display_get_width();
var current_h = display_get_height();

if (display_get_height() != window_get_height() 
    || display_get_width() != window_get_width()) {
    var current_w = window_get_width();
    var current_h = window_get_height();
}

var aspect = current_w / current_h;

if (display_get_height() != window_get_height() 
    || display_get_width() != window_get_width()) {
    aspect = window_get_width() / window_get_height();
}

var new_w;
var new_h;

if (aspect > 1)
{
    //landscape
    new_w = base_h / current_h;
    new_h = current_h;
    //display_set_gui_size(base_h * aspect, base_h);
}
else
{
    //portrait
    //display_set_gui_size(base_w, base_w / aspect);
    new_w = current_w;
    new_h = base_w / current_w;
}

display_set_gui_size(new_w, new_h);
