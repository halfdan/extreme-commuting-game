/// add_message(text, type)
var text = argument0;
var type = argument1;

ds_queue_enqueue(global.awesome_messages_queue, text);
ds_queue_enqueue(global.awesome_messages_types_queue, type);
