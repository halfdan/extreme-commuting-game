///scr_scale()
// Used BDMarvel's solution from this post: http://gmc.yoyogames.com/index.php?showtopic=575695&hl=%2Bwidth+%2Bscreen+%2Bsimulate+%2Bwindows

if (argument0 == 0) {
    device_w = display_get_width();
    device_h = display_get_height();
    
    if (os_type == os_windows)
    {
        //device_w = 1280; // Width of screen to simulate in Windows
        //device_h = 800; // Height of screen to simulate in Windows
        
        device_w = 640; // Width of screen to simulate in Windows
        device_h = 360; // Height of screen to simulate in Windows
    }
    
    window_set_size(device_w, device_h);
    display_set_gui_size(device_w, device_h);
    alarm[0] = 2; // The scaling code is handled in the alarm[0]event.

} else {
    //game_w = 1280; // These values are used to force the device to use
    //game_h = 800;// a specific aspect ratio.  16:9 in this case.
    
    game_w = 640; // These values are used to force the device to use
    game_h = 360;// a specific aspect ratio.  16:9 in this case.
    
    device_aspect = device_w / device_h;
    game_aspect = game_w / game_h; // Not used, but useful if you wish to modify the code to scale a different way.
     
    // Calculate width of view by height of device
    view_wview[0] = game_h * device_aspect;
     
    // Set height of view to height of play area
    view_hview[0] = game_h
     
    // Set width and height of view port to width and height of device
    view_wport[0] = device_w;
    view_hport[0] = device_h;
     
    // Set view offset
    offset = game_w - view_wview[0];
    offset /= 2;
    view_xview[0] = offset;
    view_yview[0] = room_height - game_h;
     
    // Turn off screen cover
    background_visible[1] = false;
}


