///my_math_round(val)
var val = argument0;

if(frac(val) < 0.5) {
    return floor(val);
} else {
    return ceil(val);
}
