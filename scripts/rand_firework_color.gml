/// rand_firework_color()
var rand = irandom_range(0, 5);
var color = c_white;

if(rand == 0) {
    color = c_white;
} else if(rand == 1) {
    color = c_red;
} else if(rand == 2) {
    color = c_orange;
} else if(rand == 3) {
    color = c_yellow;
} else if(rand == 4) {
    color = c_silver;
} else if(rand == 5) {
    color = c_blue;
}

return color;
