/// print_text_and_save_to_buffer(text, font, font_color, x, y, align, alpha, text_id)
var text = argument0;
var font = argument1;
var font_color = argument2;
var xx = argument3;
var yy = argument4;
var align = argument5;
var alpha = argument6;
var text_id = argument7;

var color = draw_get_colour();
draw_set_color(font_color);
draw_set_font(font);
draw_set_halign(align);
draw_set_alpha(alpha);
draw_text(xx, yy, text);
draw_set_color(color);
draw_set_alpha(1);

var text_pos = ds_map_find_value(obj_replay_manager.text_positions, text_id);

if (is_undefined(text_pos)) {

    var text_array = array_create(7);

    text_array[TEXT_ID] = text_id;
    text_array[TEXT_VALUE] = text;
    text_array[TEXT_FONT] = font;
    text_array[TEXT_X] = xx;
    text_array[TEXT_Y] = yy;
    text_array[TEXT_ALIGN] = align;
    text_array[TEXT_ALPHA] = alpha;
    text_array[TEXT_COLOR] = font_color;
    
    ds_list_add(obj_replay_manager.texts, text_array);
    ds_map_add(obj_replay_manager.text_positions, text_id, ds_list_size(obj_replay_manager.texts) - 1);
    
    buffer_write(obj_replay_manager.replay_buffer, buffer_u8, REP_TYPE_TEXT);
    
    buffer_write(obj_replay_manager.replay_buffer, buffer_u32, text_id);
    
    buffer_write(obj_replay_manager.replay_buffer, buffer_string, text);
    buffer_write(obj_replay_manager.replay_buffer, buffer_u8, get_font_id(font));
    buffer_write(obj_replay_manager.replay_buffer, buffer_u32, font_color);
    
    buffer_write(obj_replay_manager.replay_buffer, buffer_u16, xx);
    buffer_write(obj_replay_manager.replay_buffer, buffer_u16, yy);
    
    buffer_write(obj_replay_manager.replay_buffer, buffer_u8, align);
    buffer_write(obj_replay_manager.replay_buffer, buffer_f32, alpha);
} else {
    var old_text_array = ds_list_find_value(obj_replay_manager.texts, text_pos);
    
    //var old_text_id = old_text_array[TEXT_ID];
    var old_text = old_text_array[TEXT_VALUE];
    //var old_font = old_text_array[TEXT_FONT];
    var old_xx = old_text_array[TEXT_X];
    var old_yy = old_text_array[TEXT_Y];
    //var old_align = old_text_array[TEXT_ALIGN];
    var old_alpha = old_text_array[TEXT_ALPHA];
    //var old_font_color = old_text_array[TEXT_COLOR];
    
    buffer_write(obj_replay_manager.replay_buffer, buffer_u8, REP_TYPE_TEXT_CHANGE);
    buffer_write(obj_replay_manager.replay_buffer, buffer_u32, text_id);
    
    if ((((old_xx != xx) || (old_yy != yy))) && (old_alpha == alpha) && (old_text == text)) {
        buffer_write(obj_replay_manager.replay_buffer, buffer_u8, TEXT_CHANGE_TYPE_POS);
        buffer_write(obj_replay_manager.replay_buffer, buffer_u16, xx);
        buffer_write(obj_replay_manager.replay_buffer, buffer_u16, yy);
    } else if ((!((old_xx != xx) || (old_yy != yy))) && (old_alpha != alpha) && (old_text == text)) {
        buffer_write(obj_replay_manager.replay_buffer, buffer_u8, TEXT_CHANGE_TYPE_ALPHA);
        buffer_write(obj_replay_manager.replay_buffer, buffer_f32, alpha);
    } else if ((!((old_xx != xx) || (old_yy != yy))) && (old_alpha == alpha) && (old_text != text)) {
        buffer_write(obj_replay_manager.replay_buffer, buffer_u8, TEXT_CHANGE_TYPE_TEXT);
        buffer_write(obj_replay_manager.replay_buffer, buffer_string, text);
    } else {
        buffer_write(obj_replay_manager.replay_buffer, buffer_u8, TEXT_CHANGE_TYPE_ALL);
        buffer_write(obj_replay_manager.replay_buffer, buffer_string, text);
        buffer_write(obj_replay_manager.replay_buffer, buffer_u16, xx);
        buffer_write(obj_replay_manager.replay_buffer, buffer_u16, yy);
        buffer_write(obj_replay_manager.replay_buffer, buffer_f32, alpha);
    }
    
    old_text_array[TEXT_X] = xx;
    old_text_array[TEXT_Y] = yy;
    old_text_array[TEXT_VALUE] = text;
    old_text_array[TEXT_ALPHA] = alpha;
    
    ds_list_replace(obj_replay_manager.texts, text_pos, old_text_array);
}
