/// create_text(text, font_id, font_color, x, y, align, alpha, text_id)
var text_value = argument0;
var font_id = argument1;
var font_color = argument2;
var xx = argument3;
var yy = argument4;
var align = argument5;
var alpha = argument6;
var text_id = argument7;

var old_text_pos = ds_map_find_value(obj_replay_manager.text_positions, text_id);

var text_array = array_create(7);

text_array[TEXT_ID] = text_id;
text_array[TEXT_VALUE] = text_value;
text_array[TEXT_FONT] = find_font_by_id(font_id);
text_array[TEXT_X] = xx;
text_array[TEXT_Y] = yy;
text_array[TEXT_ALIGN] = align;
text_array[TEXT_ALPHA] = alpha;
text_array[TEXT_COLOR] = font_color;

if (is_undefined(old_text_pos)) {
    ds_list_add(obj_replay_manager.texts, text_array);
    
    ds_map_add(obj_replay_manager.text_positions, text_id, ds_list_size(obj_replay_manager.texts) - 1);
} else {
    ds_list_replace(obj_replay_manager.texts, old_text_pos, text_array);
}
