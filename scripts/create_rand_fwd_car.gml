///create_rand_fwd_car(type_to_set)
var type_to_set = argument0;
var rnd = random(100);

var start = obj_forward_car_start;

if("side" == type_to_set) {
    start = obj_forward_right_car_start;
}

var nearest_car = instance_nearest(start.x, CAR_SPAWN_POSITION_Y, obj_forward_car);
var create_instance = true;

if(nearest_car != noone) {
    if(abs(nearest_car.y - CAR_SPAWN_POSITION_Y) < MINIMUM_FORWARD_DISTANCE) {
        //Jesli juz istnieje samochod z minimalna odlegloscia od pozycji startowej, to nie tworzymy nowego.
        //Niezaleznie od pasa, po ktorym ten najblizszy samochod sie porusza.
        create_instance = false;
    }
}

var rand_x = irandom(5) -2;

if(create_instance) {
    var inst;
    
    if (rnd >= 94 && rnd <= 100) {
        inst = instance_create(start.x + rand_x, CAR_SPAWN_POSITION_Y, obj_police_car);
        
        if (random(100) > 50) {
            inst.pursuit = true;
        }
    } else {
        inst = instance_create(start.x + rand_x, CAR_SPAWN_POSITION_Y, obj_car1);
        with(inst) {
            var car_num = irandom(6);
            sprite_index = asset_get_index("spr_car_" + string(car_num))
        }
    }
    
    with(inst) {
        type = type_to_set;
    }
}


