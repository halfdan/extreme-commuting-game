///instance_number_by_type(obj, type)
var obj = argument0;
var type = argument1;

var i;
var count = 0;
for (i = 0; i < instance_number(obj); i += 1) {
    inst = instance_find(obj, i);
    if(inst.type == type) {
        count++;
    }
}

return count;
