///place_skid_marks()

var skid_type = obj_skid_mark_front;

var long_skid = (global.acc_press_time > MAX_ACC / 3);
if (long_skid) {
    skid_type = obj_skid_mark_long;
}

var pos_x = x - sprite_width / 2;
var pos_y = y - sprite_height / 2;

var tmp_skid = instance_nearest(WAIT_POSITION, WAIT_POSITION, skid_type);
if(tmp_skid == noone) {
    instance_create(pos_x, pos_y, skid_type);
} else if (!tmp_skid.waiting) {
    instance_create(pos_x, pos_y, skid_type);
} else {
    tmp_skid.waiting = false;
    tmp_skid.x = pos_x;
    tmp_skid.y = pos_y;
}

var pos_x = (x + sprite_width / 2) - sprite_get_width(spr_skid_mark_front) -2;
var pos_y = y - sprite_height / 2;

var tmp_skid = instance_nearest(WAIT_POSITION, WAIT_POSITION, skid_type);
if(tmp_skid == noone) {
    instance_create(pos_x, pos_y, skid_type);
} else if(!tmp_skid.waiting) {
    instance_create(pos_x, pos_y, skid_type);
} else {
    tmp_skid.waiting = false;
    tmp_skid.x = pos_x;
    tmp_skid.y = pos_y;
}
