///write_zoom_level(zoom)
var zoom = argument0;

buffer_write(obj_replay_manager.replay_buffer, buffer_u8, REP_TYPE_ZOOM);

buffer_write(obj_replay_manager.replay_buffer, buffer_f32, zoom);
