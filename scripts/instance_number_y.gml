///instance_number_y(obj, max_y)

var obj = argument0;
var max_y = argument1;

if(!instance_exists(obj)) {
    return noone;
}

var count = instance_number(obj);

var matching_count = 0;

for(var i = 0; i < count; i++) {
    var inst = instance_find(obj, i);
    if((inst.y < max_y && inst.y > WAIT_POSITION)) {
        continue;
    }
    matching_count++;
}

return matching_count;
