/// read_obj_from_buffer(buff_id, action_type, old_to_new_instances_map)
var buff_id = argument0;
var action_type = argument1;
var old_to_new_instances = argument2;

// here we handle all the not object related events
if (action_type == REP_TYPE_PARTICLE) {
    var particle_type_id = buffer_read(obj_replay_manager.replay_buffer, buffer_u8);
    var xx = buffer_read(obj_replay_manager.replay_buffer, buffer_u16);
    var yy = buffer_read(obj_replay_manager.replay_buffer, buffer_u16);
    var number = buffer_read(obj_replay_manager.replay_buffer, buffer_u8);
    var life_min = buffer_read(obj_replay_manager.replay_buffer, buffer_f32);
    var life_max = buffer_read(obj_replay_manager.replay_buffer, buffer_f32);
    
    create_particle(particle_type_id, xx, yy, number, life_min, life_max);
    
    return noone;
}

if (action_type == REP_TYPE_EFFECT) {
    var effect_type_id = buffer_read(obj_replay_manager.replay_buffer, buffer_u8);
    var xx = buffer_read(obj_replay_manager.replay_buffer, buffer_u16);
    var yy = buffer_read(obj_replay_manager.replay_buffer, buffer_u16);
    var size = buffer_read(obj_replay_manager.replay_buffer, buffer_f32);
    var color = buffer_read(obj_replay_manager.replay_buffer, buffer_u32);

    
    create_effect(effect_type_id, xx, yy, size, color);
    
    return noone;
}

if (action_type == REP_TYPE_TEXT) {
    var text_id = buffer_read(obj_replay_manager.replay_buffer, buffer_u32);
    
    var text = buffer_read(obj_replay_manager.replay_buffer, buffer_string);
    var font_id = buffer_read(obj_replay_manager.replay_buffer, buffer_u8);
    var font_color = buffer_read(obj_replay_manager.replay_buffer, buffer_u32);
    var xx = buffer_read(obj_replay_manager.replay_buffer, buffer_u16);
    var yy = buffer_read(obj_replay_manager.replay_buffer, buffer_u16);
    
    var align = buffer_read(obj_replay_manager.replay_buffer, buffer_u8);
    var alpha = buffer_read(obj_replay_manager.replay_buffer, buffer_f32);
    
    create_text(text, font_id, font_color, xx, yy, align, alpha, text_id);
    
    return noone;
}

if (action_type == REP_TYPE_TEXT_CHANGE) {
    var text_id = buffer_read(obj_replay_manager.replay_buffer, buffer_u32);
    
    var change_type = buffer_read(obj_replay_manager.replay_buffer, buffer_u8);
    
    if (change_type == TEXT_CHANGE_TYPE_POS) {
        var xx = buffer_read(obj_replay_manager.replay_buffer, buffer_u16);
        var yy = buffer_read(obj_replay_manager.replay_buffer, buffer_u16);
        
        update_text(TEXT_UNCHANGED_MARKER, TEXT_UNCHANGED_MARKER, xx, yy, TEXT_UNCHANGED_MARKER, text_id);
    } else if (change_type == TEXT_CHANGE_TYPE_ALPHA) {
        var alpha = buffer_read(obj_replay_manager.replay_buffer, buffer_f32);
        
        update_text(TEXT_UNCHANGED_MARKER, TEXT_UNCHANGED_MARKER, TEXT_UNCHANGED_MARKER, TEXT_UNCHANGED_MARKER, alpha, text_id);
    } else if (change_type == TEXT_CHANGE_TYPE_TEXT) {
        var text = buffer_read(obj_replay_manager.replay_buffer, buffer_string);
        
        update_text(text, TEXT_UNCHANGED_MARKER, TEXT_UNCHANGED_MARKER, TEXT_UNCHANGED_MARKER, TEXT_UNCHANGED_MARKER, text_id);
    } if (change_type == TEXT_CHANGE_TYPE_DESTROY) {
        destroy_text(text_id);
    } if (change_type == TEXT_CHANGE_TYPE_ALL) {
        var text = buffer_read(obj_replay_manager.replay_buffer, buffer_string);
        var xx = buffer_read(obj_replay_manager.replay_buffer, buffer_u16);
        var yy = buffer_read(obj_replay_manager.replay_buffer, buffer_u16);
        var alpha = buffer_read(obj_replay_manager.replay_buffer, buffer_f32);
        
        update_text(text, TEXT_UNCHANGED_MARKER, xx, yy, alpha, text_id);
    }
    
    return noone;
}

if (action_type == REP_TYPE_ZOOM) {
    var zoom = buffer_read(obj_replay_manager.replay_buffer, buffer_f32);
    scr_set_zoom_level(zoom);
    
    return noone;
}

// here are all the object related events
var obj_id = buffer_read(buff_id, buffer_u32);

if ((action_type == REP_TYPE_CREATE) || (action_type == REP_TYPE_CREATE_GUI)) {
    var xx = buffer_read(buff_id, buffer_s16);
    var yy = buffer_read(buff_id, buffer_s16);
    
    var inst_type = noone;
    
    if (action_type == REP_TYPE_CREATE) {
        inst_type = obj_replay_element;
    } else if (action_type == REP_TYPE_CREATE_GUI) {
        inst_type = obj_replay_element_gui;
    }
    
    var inst = instance_create(xx, yy, inst_type);
    
    ds_map_add(old_to_new_instances, obj_id, inst);
} else if (action_type == REP_TYPE_UPDATE) {
    var xx = buffer_read(buff_id, buffer_s16);
    var yy = buffer_read(buff_id, buffer_s16);
    
    var obj_spr_index = buffer_read(buff_id, buffer_string);
    var obj_depth = buffer_read(buff_id, buffer_s16);
    var obj_image_angle = buffer_read(buff_id, buffer_f32);
    var obj_image_alpha = buffer_read(buff_id, buffer_f32);
    var obj_image_index = buffer_read(buff_id, buffer_u8);
    var obj_image_speed = buffer_read(buff_id, buffer_f32);
    var obj_visible = buffer_read(buff_id, buffer_bool);
    
    var inst = ds_map_find_value(old_to_new_instances, obj_id);
    
    inst.x = xx;
    inst.y = yy;
    
    if (obj_spr_index != "") {
        inst.sprite_index = asset_get_index(obj_spr_index);
    }
    inst.depth = obj_depth + inst.depth_offset;
    inst.image_angle = obj_image_angle;
    inst.image_alpha = obj_image_alpha;
    inst.image_index = obj_image_index;
    inst.image_speed = obj_image_speed;
    inst.visible = obj_visible;
} else if (action_type == REP_TYPE_DESTROY) {
    var inst = ds_map_find_value(old_to_new_instances, obj_id);
    
    if(inst.object_index == obj_replayable_gui){
        test = 3;
    }
    
    instance_destroy(inst.id);
}
