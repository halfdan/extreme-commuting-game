///lerp_log(min, max, frac)
var minn = argument0;
var maxn = argument1;
var fracn = argument2;

if(fracn <= 0) return minn;

var xx = exp(abs(maxn) - abs(minn));

var result = ln((exp(abs(minn)) + xx) * fracn);

return result;
