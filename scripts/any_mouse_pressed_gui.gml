///any_mouse_pressed_gui(obj)
var obj = argument0;
for(i = 0; i < 5; i++) {
    if(!device_mouse_check_button(i, mb_left)) {
        continue;
    }
    
    var mouse_xx = display_mouse_gui_x(i);
    var mouse_yy = display_mouse_gui_y(i);
    
    if(collision_point(mouse_xx, mouse_yy, obj, false, false)) {
        return true;
    }
}

return false;
