///add_awesome_marker(text)
var text_to_set = argument0;
var instance = instance_create(obj_awesome_marker.x, obj_awesome_marker.y, obj_awesome_message);

instance.text = text_to_set;

return instance;
