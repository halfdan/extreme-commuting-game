///any_mouse_pressed(obj)
var obj = argument0;
for(i = 0; i < 5; i++) {
    if(!device_mouse_check_button(i, mb_left)) {
        continue;
    }
    
    var mouse_xx = device_mouse_x(i);
    var mouse_yy =  device_mouse_y(i);
    
    if(collision_point(mouse_xx, mouse_yy, obj, false, false)) {
        return true;
    }
}

return false;
