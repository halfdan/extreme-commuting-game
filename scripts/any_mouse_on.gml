///any_mouse_on(obj)
var obj = argument0;
for(i = 0; i < 5; i++) {
    var mouse_xx = device_mouse_x(i);
    var mouse_yy =  device_mouse_y(i);
    
    if(collision_point(mouse_xx, mouse_yy, obj, false, false)) {
        return true;
    }
}

return false;
