/// button_draw(button_id)

var _map = ds_map_find_value(global._buttonsMap, argument0);
var _type   = ds_map_find_value(_map, "type");
var _sprite = ds_map_find_value(_map, "sprite");
var _view   = ds_map_find_value(_map, "view");
var _x      = ds_map_find_value(_map, "x");
var _y      = ds_map_find_value(_map, "y");
var _scale  = ds_map_find_value(_map, "scale");
if _view > -1 {
    _x += view_xview[_view];
    _y += view_yview[_view];
}

ds_map_add(_map, "state_left", 0);
ds_map_add(_map, "state_right", 0);
ds_map_add(_map, "state_up", 0);
ds_map_add(_map, "state_down", 0);
ds_map_add(_map, "state_x", 0);
ds_map_add(_map, "state_y", 0);
ds_map_add(_map, "state", 0);

_ii = 0;
switch (_type) {
    case 0:// DPAD
        var _sr  = ds_map_find_value(_map, "state_right");
        var _sl  = ds_map_find_value(_map, "state_left");
        var _su  = ds_map_find_value(_map, "state_up");
        var _sd  = ds_map_find_value(_map, "state_down");
        if _sr > 0  _ii = 1;
        if _sl > 0  _ii = 2;
        if _su > 0  _ii = 3;
        if _sd > 0  _ii = 4;
        draw_sprite_ext(_sprite, _ii, _x, _y, _scale, _scale, 0, draw_get_color(), draw_get_alpha());    
        break;
    case 1:// STICK
        var _sx  = ds_map_find_value(_map, "state_x");
        var _sy  = ds_map_find_value(_map, "state_y");
        var _r  = ds_map_find_value(_map, "radius");
        _x += _sx * _r;
        _y += _sy * _r;
        draw_sprite_ext(_sprite, _ii, _x, _y, 1, 1, 0, draw_get_color(), draw_get_alpha());    
        break;
    case 2:// SINGLE
        var _ii  = ds_map_find_value(_map, "state");
        draw_sprite_ext(_sprite, _ii, _x, _y, _scale, _scale, 0, draw_get_color(), draw_get_alpha());    
        break;
}

return true;
