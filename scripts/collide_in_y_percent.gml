///collide_in_y_percent(other)
obj1 = argument0;

var other_real_x = obj1.x - (obj1.sprite_width / 2);
var this_real_x = x - (obj1.sprite_width / 2);

var other_width = obj1.sprite_width;
var this_width = sprite_width;

if(other_real_x > (this_real_x + this_width)) {
    return 0;
} else if ((other_real_x + other_width) < this_real_x) {
    return 0;
}

var common_width = 0;
if(other_real_x > this_real_x) {
    common_width = (this_real_x + this_width) - other_real_x;
} else {
    common_width = (other_real_x + other_width) - this_real_x;
}

return max(common_width/other_width, common_width/this_width);
