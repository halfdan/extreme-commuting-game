/// console_input_ready(name)
var name = argument0;

ini_open(INI_NAME);
    ini_write_string( 'Leaderboards', 'Name', name);
    ini_write_real( 'Leaderboards', 'Score', global.new_leader_score);

ini_close();

obj_leader_name.leader_name = name;

obj_leaderboard_room_controller.click_count = 0;
obj_leaderboard_room_controller.press_count = 0;

return false;
