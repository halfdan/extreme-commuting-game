///display_mouse_gui_x(pos_x)
var pos_x = argument0;
if (os_type == os_windows || os_type == os_linux || os_type == os_macosx) {
    return display_get_gui_width() * (device_mouse_raw_x(pos_x) / window_get_width());
} else {
    return display_get_gui_width() * (device_mouse_raw_x(pos_x) / display_get_width());
}
