///scr_set_zoom_level(zoom)
var zoom = argument0;
view_wview[0] = (obj_util_screen_res.game_h + zoom) * obj_util_screen_res.device_aspect;
view_hview[0] = (obj_util_screen_res.game_h + zoom)

// Set view offset
obj_util_screen_res.offset = obj_util_screen_res.game_w - view_wview[0];
obj_util_screen_res.offset /= 2;

var shake = 0;

if (instance_exists(obj_car_player)) {
    if (obj_car_player.turbo > 0) {
        shake = random((zoom - 30)/20) - (((zoom-30)/20)/2);
    }
    global.turbo_view_up = lerp(global.turbo_view_up, obj_car_player.turbo * 2, 0.5);
}

var crash_shake = 0;
if (global.crash_screen_shake_counter > 0) {
    global.crash_screen_shake_counter--;
    
    crash_shake = random(10) - 5;
}

view_xview[0] = obj_util_screen_res.offset + shake + crash_shake;

view_yview[0] = room_height - obj_util_screen_res.game_h - 1.1*zoom + global.turbo_view_up;
