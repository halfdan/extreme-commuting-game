/// button_get_state(button_id,{...})

var _map = ds_map_find_value(global._buttonsMap, argument0);
var _type   = ds_map_find_value(_map, "type");

var _ret = 0;
switch (_type) {
    case 0:// DPAD
        if (argument1 == gp_padl)
            _ret = ds_map_find_value(_map, "state_left");
        if (argument1 == gp_padr)
            _ret  = ds_map_find_value(_map, "state_right");
        if (argument1 == gp_padu)
            _ret  = ds_map_find_value(_map, "state_up");
        if (argument1 == gp_padd)
            _ret  = ds_map_find_value(_map, "state_down");
        break;
    case 1:// STICK
        if (argument1 == gp_axislh || argument1 == gp_axisrh)
            _ret  = ds_map_find_value(_map, "state_x");
        if (argument1 == gp_axislv || argument1 == gp_axisrv)
            _ret  = ds_map_find_value(_map, "state_y");
        break;
    case 2:// SINGLE
        var _ret  = ds_map_find_value(_map, "state");
        break;
}

return _ret;
