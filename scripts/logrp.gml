///logrp(minimum, maximum, fraction)
var minimum = argument0;
var maximum = argument1;
var fraction = argument2;

var min_exp = exp(minimum);
var max_exp = exp(maximum);

var xx = max_exp - min_exp;

var result = ln(minimum + (xx * fraction));

return result;
//return lerp(minimum, maximum, value);
