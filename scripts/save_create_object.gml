/// save_create_object([is_gui])
var is_gui = argument0;

if (is_gui) {
    write_obj_to_buffer(obj_replay_manager.replay_buffer, REP_TYPE_CREATE_GUI);
} else {
    write_obj_to_buffer(obj_replay_manager.replay_buffer, REP_TYPE_CREATE);
}
