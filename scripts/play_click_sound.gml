///play_click_sound()

if(audio_is_playing(snd_key_click)) {
    audio_stop_sound(snd_key_click);
}
audio_play_sound(snd_key_click, 0, false);
audio_sound_pitch(snd_key_click, 1 + random_range(-0.2, 0.2));
