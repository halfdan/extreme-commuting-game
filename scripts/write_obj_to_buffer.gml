///write_obj_to_buffer(buf_index, action_type)

if ((obj_replay_manager.replay_step_no < REPLAY_MAX_FRAMES) or argument1 == REP_TYPE_EOF) {
    var buf_index = argument0;
    var action_type = argument1;
    
    if ((action_type == REP_TYPE_CREATE) || (action_type == REP_TYPE_CREATE_GUI)) {
        buffer_write(buf_index, buffer_u8, action_type);
        buffer_write(buf_index, buffer_u32, id);
        buffer_write(buf_index, buffer_s16, x);
        buffer_write(buf_index, buffer_s16, y);
    } else if (action_type == REP_TYPE_UPDATE) {
        buffer_write(buf_index, buffer_u8, REP_TYPE_UPDATE);
        buffer_write(buf_index, buffer_u32, id);
        buffer_write(buf_index, buffer_s16, x);
        buffer_write(buf_index, buffer_s16, y);
        
        buffer_write(buf_index, buffer_string, sprite_get_name(sprite_index));
        buffer_write(buf_index, buffer_s16, depth);
        buffer_write(buf_index, buffer_f32, image_angle);
        buffer_write(buf_index, buffer_f32, image_alpha);
        buffer_write(buf_index, buffer_u8, image_index);
        buffer_write(buf_index, buffer_f32, image_speed);
        buffer_write(buf_index, buffer_bool, visible);
    } else if (action_type == REP_TYPE_DESTROY) {
        buffer_write(buf_index, buffer_u8, REP_TYPE_DESTROY);
        buffer_write(buf_index, buffer_u32, id);
    } else if (action_type == REP_TYPE_STEP) {
        buffer_write(buf_index, buffer_u8, REP_TYPE_STEP);
    } else if (action_type == REP_TYPE_EOF) {
        buffer_write(buf_index, buffer_u8, REP_TYPE_EOF);
    }
}
