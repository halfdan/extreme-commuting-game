/// read_level_data(level_number)
var level_number = argument0;

var file = file_text_open_read(working_directory + "\level" + string(level_number) + ".json");

var json_str = "";
while (!file_text_eof(file)) {
   json_str += file_text_readln(file);
}
file_text_close(file);

return json_decode(json_str);
