/// add_achievement(ach_id, ach_image, ach_amount, notification)
ach_id = argument0;
ach_image = argument1;
ach_amount = argument2;
notification = argument3;

if (is_running_on_mobile()) {
    if (!instance_exists(obj_android_achievements_manager)) {
        instance_create(-1000, -1000, obj_android_achievements_manager);
    }
    
    ds_queue_enqueue(obj_android_achievements_manager.ach_ids, ach_id);
    ds_queue_enqueue(obj_android_achievements_manager.ach_amounts, ach_amount);
} else {
    ini_open(INI_NAME);
        var current_val = ini_read_real("Achievements", ach_id, 0);
        
        if (current_val < 100) {
            if (notification) {
                var inst = instance_create(50, 50, obj_achievement_notificon);
                inst.sprite_index = ach_image;
                inst.percent = current_val;
                inst.added_percent = min(ach_amount, 100 - current_val);
                
                audio_play_sound(snd_ach_ding, 0, false);
                audio_sound_pitch(snd_ach_ding, ((current_val + ach_amount + 25)/100) * 1.5);
            }
            
            current_val += ach_amount;
            
            if(current_val > 100) {
                current_val = 100;
            }
            
            ini_write_real("Achievements", ach_id, current_val);
        }

    ini_close();
}
