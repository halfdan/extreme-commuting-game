/// get_car_chance(car_type, backwards)
var car_type = argument0;
var backwards = argument1;

if (is_level_random()) {
    if (backwards) {
        return BACKWARD_CAR_CHANCE;
    } else {
        return FORWARD_CAR_CHANCE;
    }
}

var data_key = "";
if (backwards) {
    data_key = "backwards_";
} else {
    data_key = "forwards_";
}
if (car_type == "side") {
    data_key += "side_";
}
data_key += "traffic_chance";

var data = get_level_data_entry(data_key);
var counter = 0;

if (backwards) {
    if (car_type == "side") {
        counter = obj_levels_controller.backwards_side_traffic_chance_counter;
    } else {
        counter = obj_levels_controller.backwards_traffic_chance_counter;
    }
} else {
    if (car_type == "side") {
        counter = obj_levels_controller.forwards_side_traffic_chance_counter;
    } else {
        counter = obj_levels_controller.forwards_traffic_chance_counter;
    }
}

if (ds_list_size(data) - 1 >= counter) {
    return ds_list_find_value(data, counter);
} else {
    return 0;
}
