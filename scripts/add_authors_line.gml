///add_authors_line(x, y, text, type)
var xx = argument0;
var yy = argument1;
var text = argument2;
var type = argument3;

var inst = instance_create(xx, yy, obj_authors_text);

inst.x = xx;
inst.y = yy;
inst.text = text;
inst.type = type;
