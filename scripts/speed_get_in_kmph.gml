///speed_get_in_kmph(vel)
var vel = argument0;

//z proporcji
return MIN_PRESENTATION_SPEED + (((vel - MIN_POSSIBLE_ROAD_SPEED) * (MAX_PRESENTATION_SPEED - MIN_PRESENTATION_SPEED))
        / 
        (MAX_POSSIBLE_ROAD_SPEED - MIN_POSSIBLE_ROAD_SPEED));
