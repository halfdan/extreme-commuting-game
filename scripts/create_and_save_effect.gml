/// create_and_save_effect(effect_type_id, x, y, size, color)
var effect_type_id = argument0;
var xx = argument1;
var yy = argument2;
var size = argument3;
var color = argument4;

create_effect(effect_type_id, xx, yy, size, color);

buffer_write(obj_replay_manager.replay_buffer, buffer_u8, REP_TYPE_EFFECT);

buffer_write(obj_replay_manager.replay_buffer, buffer_u8, effect_type_id);
buffer_write(obj_replay_manager.replay_buffer, buffer_u16, xx);
buffer_write(obj_replay_manager.replay_buffer, buffer_u16, yy);
buffer_write(obj_replay_manager.replay_buffer, buffer_f32, size);
buffer_write(obj_replay_manager.replay_buffer, buffer_u32, color);
