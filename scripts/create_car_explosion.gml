///create_car_explosion()
create_and_save_effect(EFFECT_EXPLOSION, x, y, 1, c_yellow);
create_and_save_effect(EFFECT_SMOKE, x, y, 1, c_gray);
create_and_save_effect(EFFECT_FIREWORK, x, y, 1, c_orange);
create_and_save_effect(EFFECT_FIREWORK, x, y, 0.5, c_red);
create_and_save_effect(EFFECT_FLARE, x, y, 1, c_white);
