/*

// DOCUMENTATION

button_init();
    Description:
        Should be called prior to any other button_ function to set up the engine.


button_create(button_type, key, sprite, view, x, y, scale);
    Arguments:
        button_type: the type of button you want to create;
            0 = Dpad
            1 = Stick
            2 = Normal
        key: Sets a key to emulate when pressing the given button
            Beware that this only works on 'normal' types
        sprite: The sprite to use when drawing
        view: if you want all drawing to be made relative to a view, set
            its ID here, else just use -1
        x: x position
        y: y position
        scale: drawing scale
    
    Description:
        Creates a new button and returns its ID to use used on other functions


button_draw(button_id);
    Arguments:
        button_id:
            The button ID returned when creating this button
    
    Description:
        Draws the given button


button_get_state(button_id);
    Arguments:
        button_id:
            The button ID returned when creating this button
    
    Description:
        Returns this button pressing state;
            0 = Idle
            1 = Just pressed
            2 = Holding
            3 = Just released


button_set_position(button_id,view,x,y);
    Arguments:
        button_id:
            The button ID returned when creating this button
        view: The view you want this button position to be relative to
            if you dont need this just set -1
        x: x position
        y: y position
    
    Description:
        Returns this button pressing state;
            0 = Idle
            1 = Just pressed
            2 = Holding
            3 = Just released


button_get_x(button_id);
    Arguments:
        button_id:
            The button ID returned when creating this button
    
    Description:
        Returns this button X position


button_get_y(button_id);
    Arguments:
        button_id:
            The button ID returned when creating this button
    
    Description:
        Returns this button Y position


button_set_sprite(button_id, sprite);
    Arguments:
        button_id:
            The button ID returned when creating this button
        sprite:
            The sprite to set
    
    Description:
        Sets or changes this button sprite


button_update(button_id);
    Arguments:
        button_id:
            The button ID returned when creating this button
    
    Description:
        Updates the given button's logic; this includes button presses,
            states, etc. It needs to be called only once per step.
        Calling this multiple times will ave no effect on the engine
            altrough it will slow things down.


button_update_all();   
    Description:
        Same as above, trough this function updates ALL buttons, even those
            not being currently used.

*/

