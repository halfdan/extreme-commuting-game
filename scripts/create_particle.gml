/// create_particle(particle_type_id, x, y, number, life_min, life_max)

var particle_type_id = argument0;
var xx = argument1;
var yy = argument2;
var number = argument3;
var life_min = argument4;
var life_max = argument5;

var particle_type = ds_map_find_value(obj_particles_controller.part_types, particle_type_id);

if ((life_min != 0) || (life_max != 0)) {
    part_type_life(particle_type, life_min, life_max);
}

part_particles_create(obj_particles_controller.part_system, xx, yy, particle_type, number);

