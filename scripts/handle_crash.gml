///handle_crash()
stop_action();
stop_cars_behind_player();

global.crash_screen_shake_counter = 30;

var awesome_lost = global.awesome_tmp * global.awesome_multiplier;

var i;
for (i = 0; i < instance_number(obj_awesome_message); i ++) {
    message = instance_find(obj_awesome_message, i);
    message.crashed = true;
    
    ds_queue_clear(global.awesome_messages_queue);
    ds_queue_clear(global.awesome_messages_types_queue);
}

lives -= 1;

global.awesome_total_lost += awesome_lost;

if(awesome_lost >= 10000) {
    audio_play_sound(snd_crowd_disappointed, 0, false);
}

global.awesome_multiplier = 0;
global.awesome += global.awesome_tmp;
global.awesome_tmp = 0;

if (awesome_lost > 0) { 

    var life_bonus = '';
    if (awesome_lost > 100000) {
        life_bonus = ' +1UP';
    } else if (awesome_lost > 500000) {
        life_bonus = ' +2UP';
    }

    add_message("Lost " + string(awesome_lost) + life_bonus, "WARN");
}
