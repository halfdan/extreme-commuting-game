/// button_set_position(button_id,view,x,y);

var _map = ds_map_find_value(global._buttonsMap, argument0);

ds_map_replace(_map, "view", argument1);
ds_map_replace(_map, "x", argument2);
ds_map_replace(_map, "y", argument3);

return true;
