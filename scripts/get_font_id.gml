/// get_font_id(font)
var font = argument0;

if (font == fnt_digits) {
    return FONT_DIGITS;
}
if (font == fnt_chunk_five_20) {
    return FONT_CHUNK_FIVE_20;
}
if (font == fnt_chunk_five_30) {
    return FONT_CHUNK_FIVE_30;
}
if (font == fnt_chunk_five_40) {
    return FONT_CHUNK_FIVE_40;
}
if (font == fnt_chunk_five_50) {
    return FONT_CHUNK_FIVE_50;
}
