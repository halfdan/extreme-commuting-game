/// destroy_write_text(text_id)
var text_id = argument0;

buffer_write(obj_replay_manager.replay_buffer, buffer_u8, REP_TYPE_TEXT_CHANGE);
buffer_write(obj_replay_manager.replay_buffer, buffer_u32, text_id);
buffer_write(obj_replay_manager.replay_buffer, buffer_u8, TEXT_CHANGE_TYPE_DESTROY);
