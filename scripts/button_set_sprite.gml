/// button_set_sprite(button_id,sprite)

var _map = ds_map_find_value(global._buttonsMap, argument0);

ds_map_replace(_map, "sprite", argument1);

return true;
