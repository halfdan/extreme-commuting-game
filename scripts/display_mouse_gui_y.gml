///display_mouse_gui_y(pos_y)
var pos_y = argument0;
if (os_type == os_windows || os_type == os_linux || os_type == os_macosx) {
    return display_get_gui_height() * (device_mouse_raw_y(pos_y) / window_get_height());
} else {
    return display_get_gui_height() * (device_mouse_raw_y(pos_y) / display_get_height());
}
