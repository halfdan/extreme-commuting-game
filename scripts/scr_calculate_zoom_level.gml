///scr_calculate_zoom_level(speed)
var _speed = argument0;

var percent = ((_speed - MIN_PRESENTATION_SPEED) / (MAX_PRESENTATION_SPEED - MIN_PRESENTATION_SPEED));

return lerp(0, MAX_ZOOM, percent);
