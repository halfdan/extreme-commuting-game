/// create_effect(effect_type, x, y, size, color)
var effect_type = argument0;
var xx = argument1;
var yy = argument2;
var size = argument3;
var color = argument4;

var ef_type = noone;

switch (effect_type) {
    case EFFECT_EXPLOSION : ef_type = ef_explosion; break;
    case EFFECT_SMOKE : ef_type = ef_smoke; break;
    case EFFECT_FIREWORK : ef_type = ef_firework; break;
    case EFFECT_FLARE : ef_type = ef_explosion; break;
}

effect_create_above(ef_type, xx, yy, size, color);

