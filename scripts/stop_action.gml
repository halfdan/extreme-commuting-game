global.car_crashed = true;
global.speed = MIN_SPEED;

if (audio_is_playing(obj_car_player.engine_sound)) {
    audio_stop_sound(obj_car_player.engine_sound);
}

if (audio_is_playing(obj_car_player.engine_sound_stop)) {
    audio_play_sound(obj_car_player.engine_sound_stop, 1, false);
}

audio_play_sound(global.crash_sounds[irandom_range(0, 2)], 1, false);
